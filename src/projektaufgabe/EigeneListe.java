package projektaufgabe;

public class EigeneListe {

    private Listenobjekt ersteElement;

    EigeneListe() {
    }

    EigeneListe(String text) {
        ersteElement = new Listenobjekt(text);
    }

    private Listenobjekt letzte() {
        Listenobjekt tempObjekt = this.ersteElement;
        while (tempObjekt.getNext() != null) {
            tempObjekt = tempObjekt.getNext();
        }
        return tempObjekt;
    }

    public void ergaenzeAmEnde(String text) {
        if (this.ersteElement == null) {
            this.ersteElement = new Listenobjekt();
            this.ersteElement.setEintrag(text);
            this.ersteElement.setIndex(0);
        } else {
            Listenobjekt letztesElement = letzte();
            letztesElement.setNext(new Listenobjekt(text));
            letztesElement.getNext().setIndex(letztesElement.getIndex() + 1);
        }
    }

    public void loescheLetzte() {
        Listenobjekt liste = this.ersteElement;
        while (liste.getNext().getNext() != null) {
            liste = liste.getNext();
        }
        liste.setNext(null);
    }

    public String ausgebenText() {
        String ausgabe = "";
        Listenobjekt liste = this.ersteElement;

        if (liste == null) {
            return "";
        }
        ausgabe = this.ersteElement.getEintrag();
        liste = liste.getNext();
        while (liste != null) {
            ausgabe = ausgabe + " " + liste.getEintrag();
            liste = liste.getNext();
        }
        return ausgabe;
    }

    public void ergaenzeAmAnfang(String text) {
        Listenobjekt buffer = this.ersteElement;
        this.ersteElement = new Listenobjekt(text);
        this.ersteElement.setNext(buffer);

        //Indizes erhöhen
        buffer = this.ersteElement;
        while (buffer.getNext() != null) {
            buffer = buffer.getNext();
            buffer.setIndex(buffer.getIndex() + 1);
        }
    }

    public void einfuegenBeiIndex(String text, int index) {
        Listenobjekt einfuegenNach = this.ersteElement;
        int anzahlDurchlaeufe = index - 1;

        //zur richtigen Stelle laufen und Objekt einfügen
        for (int i = 0; i < anzahlDurchlaeufe; i++) {
            einfuegenNach = einfuegenNach.getNext();
        }
        Listenobjekt bufferObjekt = einfuegenNach.getNext();
        einfuegenNach.setNext(new Listenobjekt(text));
        einfuegenNach.getNext().setIndex(einfuegenNach.getIndex() + 1);
        einfuegenNach.getNext().setNext(bufferObjekt);

        //Indizes erhöhen
        int bufferIndex = einfuegenNach.getIndex() + 1;
        while (bufferObjekt != null) {
            bufferObjekt.setIndex(bufferIndex + 1);
            bufferIndex += 1;
            bufferObjekt = bufferObjekt.getNext();
        }
    }

    public Listenobjekt holeElement(int index) {
        Listenobjekt ausgabe = this.ersteElement;
        for (int i = 0; i < index; i++) {
            ausgabe = ausgabe.getNext();
        }
        return ausgabe;
    }

    public int erstesVorkommen(String suchtext) {
        int ausgabe = -1;
        Listenobjekt bufferObjekt = this.ersteElement;
        while (bufferObjekt != null && ausgabe == -1) {
            if (bufferObjekt.getEintrag().equals(suchtext)) {
                ausgabe = bufferObjekt.getIndex();
            }
            bufferObjekt = bufferObjekt.getNext();
        }
        return ausgabe;
    }

    public int letztesVorkommen(String suchtext) {
        int ausgabe = -1;
        Listenobjekt bufferObjekt = this.ersteElement;
        while (bufferObjekt != null) {
            if (bufferObjekt.getEintrag().equals(suchtext)) {
                ausgabe = bufferObjekt.getIndex();
            }
            bufferObjekt = bufferObjekt.getNext();
        }
        return ausgabe;
    }

}
