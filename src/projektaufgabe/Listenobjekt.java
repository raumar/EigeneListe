package projektaufgabe;


public class Listenobjekt {

    private int index;
    private String eintrag;
    private Listenobjekt next;

    Listenobjekt() {
        this.setIndex(0);
    }

    Listenobjekt(String text) {
        this.setEintrag(text);
        this.setIndex(0);
    }


    public void setIndex(int index) {
        this.index = index;
    }

    public void setEintrag(String eintrag) {
        this.eintrag = eintrag;
    }

    public int getIndex() {
        return index;
    }

    public String getEintrag() {
        return eintrag;
    }

    public Listenobjekt getNext() {
        return next;
    }

    public void setNext(Listenobjekt next) {
        this.next = next;
    }
}
